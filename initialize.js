(function() {

    var global = this;
    var GetTheLook = (global.GetTheLook || (global.GetTheLook = {}));
    
    var Core = GetTheLook.Core = function(options) {

        var defaults = {};
        this.config = $.extend(true, defaults, options || {});
        
        $('.panel').each(function() {
            new GetTheLook.Panel({
                panel: this
            });
        });
        
        global.urlHandler = new GetTheLook.URLHandler();
    }
    
}).call();
