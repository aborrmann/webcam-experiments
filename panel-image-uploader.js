(function() {
    
    var global = this;
    var TweenMax = global.TweenMax
    var GetTheLook = (global.GetTheLook || (global.GetTheLook = {}));
    
    var ImageUploader = GetTheLook.ImageUploader = function(options) {
        
        var defaults = {
            imageKey: 'getthelook.uploadedimage'
        };
        this.config = $.extend(true, defaults, options || {});
        this.fileReader = null;

        this._onImageChosenProxied = $.proxy(this._onImageChosen, this);
        this._onImageLoadedProxied = $.proxy(this._onImageLoaded, this);
    };
    
    
    ImageUploader.prototype.activate = function() {
        
        console.log('activate image uploader');

        var $imagePicker = $(this.config.panel).find('.image-picker');
        $imagePicker.on('change', this._onImageChosenProxied);

        this._showImage();
    };
    
    
    ImageUploader.prototype.deactivate = function() {
        
        console.log('deactivate image uploader');
        $(this.fileReader).off('load', this._onImageLoadedProxied);
        this.fileReader = null;
    };


    ImageUploader.prototype._onImageChosen = function(e) {

        var files = event.target.files;
        if (files.length > 0) {

            this.fileReader = new FileReader();
            $(this.fileReader).on('load', this._onImageLoadedProxied);
            this.fileReader.readAsDataURL(files[0]);
        }
        else {
            console.log('no image chosen');
        }
    };


    ImageUploader.prototype._onImageLoaded = function(e) {

        global.localStorage[this.config.imageKey] = e.target.result;
        this._showImage();
    }


    ImageUploader.prototype._showImage = function() {

        var $image = $(this.config.panel).find('.uploaded-image');
        $image.attr('src', global.localStorage[this.config.imageKey]);
    };
    
}).call();