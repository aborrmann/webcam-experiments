(function() {
    
    var global = this;
    var TweenMax = global.TweenMax
    var GetTheLook = (global.GetTheLook || (global.GetTheLook = {}));
    
    var Instructions = GetTheLook.Instructions = function(options) {
        
        var defaults = {};
        this.config = $.extend(true, defaults, options || {});
    };
    
    
    Instructions.prototype.activate = function() {
        
        console.log('activate instructions');
        $(this.config.panel).find('button').on('click', this._onButtonClick);
    };
    
    
    Instructions.prototype.deactivate = function() {
        
        console.log('deactivate instructions');
        $(this.config.panel).find('button').off('click', this._onButtonClick);
    };


    Instructions.prototype._onButtonClick = function(e) {

        var newHash = urlHandler.getHash()[2];
        urlHandler.setHash([newHash]);
    };
    
}).call();