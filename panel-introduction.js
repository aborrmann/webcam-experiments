(function() {
    
    var global = this;
    var TweenMax = global.TweenMax
    var GetTheLook = (global.GetTheLook || (global.GetTheLook = {}));
    
    var Introduction = GetTheLook.Introduction = function(options) {
        
        var defaults = {};
        this.config = $.extend(true, defaults, options || {});
    };
    
    
    Introduction.prototype.activate = function() {
        
        $('#UseWebcam')[ this._getWebcam() ? 'show' : 'hide' ]();
    };
    
    
    Introduction.prototype.deactivate = function() {
        
        
    };


    Introduction.prototype._getWebcam = function() {

        navigator.getUserMedia = 
            navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia
        ;

        return navigator.getUserMedia;
    }
    
}).call();