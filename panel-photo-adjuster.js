(function() {
    
    var global = this;
    var TweenMax = global.TweenMax
    var GetTheLook = (global.GetTheLook || (global.GetTheLook = {}));
    
    var PhotoAdjuster = GetTheLook.PhotoAdjuster = function(options) {
        
        var defaults = {};
        this.config = $.extend(true, defaults, options || {});

        if (!localStorage['getthelook.imagerotation']) {
            localStorage['getthelook.imagerotation'] = 0;
        }

        if (!localStorage['getthelook.imagescale']) {
            localStorage['getthelook.imagescale'] = 1.0;
        }
    };
    
    
    PhotoAdjuster.prototype.activate = function() {

        this.config.photo = global.localStorage['getthelook.uploadedimage'];
        $('.uploaded-image').attr('src', this.config.photo);
        this.adjustImage();

        $('.btn.rotate-left').on('click', $.proxy(this._rotateLeft, this));
        $('.btn.rotate-right').on('click', $.proxy(this._rotateRight, this));

        $('.btn.zoom-in').on('click', $.proxy(this._zoomIn, this));
        $('.btn.zoom-out').on('click', $.proxy(this._zoomOut, this));

        $('.uploaded-image').on('mousedown', this._startPan);
    };
    
    
    PhotoAdjuster.prototype.deactivate = function() {
        
        $('.uploaded-image').attr('src', '');
        this.config.photo = null;
 
        $('.btn.rotate-left').off('click');
        $('.btn.rotate-right').off('click');

        $('.btn.zoom-in').off('click');
        $('.btn.zoom-out').off('click');
    };


    PhotoAdjuster.prototype._startPan = function(e) {

        $(this).on('mouseup', function() {
            $(this).off('mousemove');
        });

        $(this).on('mousemove', function() {

            console.log('mousemove');
        });
    };


    PhotoAdjuster.prototype._rotateRight = function(e) {

        localStorage['getthelook.imagerotation'] = 
            parseInt(localStorage['getthelook.imagerotation']) + 1;

        this.adjustImage();
    };


    PhotoAdjuster.prototype._rotateLeft = function(e) {

        localStorage['getthelook.imagerotation'] = 
            parseInt(localStorage['getthelook.imagerotation']) - 1;

        this.adjustImage();
    };


    PhotoAdjuster.prototype._zoomIn = function(e) {

        localStorage['getthelook.imagescale'] =
            (parseFloat(localStorage['getthelook.imagescale']) + 0.05)
                .toFixed(2);

        this.adjustImage();
    };


    PhotoAdjuster.prototype._zoomOut = function(e) {

        localStorage['getthelook.imagescale'] =
            (parseFloat(localStorage['getthelook.imagescale']) - 0.05)
                .toFixed(2);

        this.adjustImage();
    };



    PhotoAdjuster.prototype.adjustImage = function() {

        TweenMax.to(
            $('.uploaded-image'),
            0.15,
            {
                rotation: parseInt(localStorage['getthelook.imagerotation']),
                scale: parseFloat(localStorage['getthelook.imagescale'])
            }
        );
    };
    
}).call();