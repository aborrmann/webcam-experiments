(function() {

    var global = this;
    var TweenMax = global.TweenMax
    var GetTheLook = (global.GetTheLook || (global.GetTheLook = {}));
    
    var Panel = GetTheLook.Panel = function(options) {
        
        var defaults = {};
        this.config = $.extend(true, defaults, options || {});
        this.config.slug = $(this.config.panel).attr('name');


        this.config.controllerName = $(this.config.panel).data('controller');
        console.log('Controller Name: ' + this.config.controllerName);
        if (this.config.controllerName) {

            // Do a quick checkt to make sure the controller implements the required methods
            var controllerPrototype = GetTheLook[this.config.controllerName].prototype;
            if ( !(
                controllerPrototype.activate &&
                controllerPrototype.deactivate
            )) throw new Error(this.config.controllerName + ' controller does not implement the required methods!');

            this.config.controller = new GetTheLook[this.config.controllerName]({
                panel: this.config.panel
            });
        }

        
        this.initialize();
        $(GetTheLook).bind('panel_change', $.proxy(this._onPanelChange, this));
        $(window).on('resize', $.proxy(this.initialize, this));
    };
    
    
    Panel.prototype.initialize = function() {
        
        if (!this.isActive) {
            TweenMax.set(
                this.config.panel,
                { x: window.innerWidth }
            );
            this.isActive = false;
        }
    }
    
    
    Panel.prototype._onPanelChange = function(e, panel_id) {
        
        if (this.config.slug == panel_id) {
            this._setActive();
        }
        else {
            this._setInactive();
        }
    };
    
    
    Panel.prototype._setActive = function() {
        
        if (!this.isActive) {
            
            TweenMax.to(
                this.config.panel,
                0.33,
                { x: 0 }
            );
            
            if (this.config.controller) {
                this.config.controller.activate();
            }

            this.isActive = true;
        }
    };
    
    
    Panel.prototype._setInactive = function() {
        
        if (this.isActive) {
            TweenMax.to(
                this.config.panel,
                0.33,
                {
                    x: -window.innerWidth,
                    onComplete: this.initialize,
                    onCompleteScope: this
                }
            );
            
            if (this.config.controller) {
                this.config.controller.deactivate();
            }
            
            this.isActive = false;
        }
    };
    
}).call();