(function() {
    
    var global = this;
    var GetTheLook = (global.GetTheLook || (global.GetTheLook = {}));
    
    var URLHandler = GetTheLook.URLHandler = function(options) {
        
        var defaults = {
            defaultSlug: 'introduction',
            baseHash: '!',
            separator: '/'
        };
        this.config = $.extend(true, defaults, options || {});

        window.onhashchange = $.proxy(this._onHashChange, this);
        
        if (window.location.hash) {
            this._onHashChange();
        }
        else {
            this.setHash([this.config.defaultSlug]);
        }
    };


    URLHandler.prototype.getHash = function() {

        return window.location.hash.split(this.config.separator);
    }
    

    /* 
    Accepts a `path` array of strings to form the hash URL
    */
    URLHandler.prototype.setHash = function(path) {

        var hash = this.config.separator;
        for (var i in path) {
            hash += path[i] + this.config.separator;
        }
        
        window.location.hash = this.config.baseHash + hash;
    };
    
    
    URLHandler.prototype._onHashChange = function() {
        
        var path = this.getHash();
        console.log(path);
        
        // The 0th level of the hashtag is this.config.baseHash
        
        // The first level of hash changes corresponds to a panel change
        var panel_id = path[1];
        if (panel_id) {
            $(GetTheLook).trigger('panel_change', path[1]);
        }
    };
    
}).call();