var imagePicker = document.getElementById('ImagePicker');
imagePicker.onchange = function(e) {
    var files = event.target.files;
    if (files.length > 0) {
        loadImage(files[0]);
    }
    else {
        alert('didnt get a file');
    }
}


function loadImage(file) {
    
    var reader = new FileReader();
    var image = document.getElementById('Photo');
    
    reader.onload = function(e) {
        image.src = e.target.result;
    }
    reader.readAsDataURL(file);
}



var overlay = document.getElementById('Overlay');
var plainbtn = document.getElementById('Plain');
var bluebtn = document.getElementById('Blue');
var redbtn = document.getElementById('Red');

plainbtn.onclick = function() {
    console.log('plain button click');
    overlay.className = '';
}
redbtn.onclick = function() {
    console.log('red button click');
    overlay.className = 'red';
}
bluebtn.onclick = function() {
    console.log('blue button click');
    overlay.className = 'blue';
}



navigator.getUserMedia  = navigator.getUserMedia ||
                          navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia ||
                          navigator.msGetUserMedia;

var video = document.getElementById('Video');

if (!video) {
    console.log('no video element found');
}

function errorCallback(e) {
    console.log('getusermedia error');
}

if (navigator.getUserMedia) {
    console.log('has user media');
    navigator.getUserMedia({audio: false, video: true}, function(stream) {
        console.log('user media gotten', stream);
        video.src = window.URL.createObjectURL(stream);
        video.play();
        video.onloadedmetadata = function(e) {
            console.log('video metadata loaded');
            
            /*
            var canvas = document.createElement('canvas');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            var ctx = canvas.getContext('2d');
            ctx.drawImage(video, 0, 0);
            overlay.appendChild(canvas);
            */

            // Stop the stream, immediately after 
            stream.stop();
        };
    }, errorCallback);
}
else {
  console.log('no video');
}

var errorCallback = function(e) {
    console.log('video rejected', e);
};
